#include <stdio.h>

#define UNUSED(x) (void)(x)

int mon_strlen_tab(char s[]){
int i=0;
    for(i=0;s[i]!='\0';i++){}
        return i;
}
int mon_strlen(const char *s){
    const char *idx=s;
    while(*s!='\0'){
        s++;
        
    }
    return s-idx;
}
int mon_strcmp(const char * s1, const char * s2)
{
	while(*s1 != '\0' && *s2 != '\0' && *(s1) == *(s2))
	{
		s1++;
		s2++;
	}
	return *s1 - *s2;
}

int mon_strncmp(const char * s1, const char * s2, int n) {
	if(n <= 0)
		return 0;
	while(--n && *s1 && *s2 && (*s1 == *s2))
	{
		s1++;
		s2++;
	}

	return *s1-*s2;
}

char *mon_strcat(char *s1, const char *s2){
int i=mon_strlen(s1);
    while(*s2!='\0'){
    s1[i++] = *(s2++);
    }
    s1[i]=*s2;
    return s1;
}
char *mon_strchr(const char *s, char c){
    const char *res = s;
    for(int i=0;*res!='\0';i++){
        if(*res==c){
            return (char *)res;
        }
        res++;
    }
    return NULL;
}
char *mon_strstr(const char *haystack,const char *needle){
    char * res = (char *) &(*haystack);
    if(mon_strlen(needle) == 0){
        return res;
    }
    for(int i = 0; haystack[i]!='\0'; i++) {
        if(mon_strncmp(&haystack[i], needle, mon_strlen(needle)) == 0) {
            res = (char *) &haystack[i];
            return res;
        }
    }
    return NULL;
}





