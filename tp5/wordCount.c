#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

int traiter (int f, int *car, int *mot, int *lig) {
	char buffer[80];
	if(f == -1){
	  printf("N'a pas trouvé le fichier");
	  return -1;
	}
	else{
		char last = ' ';
		int nb = read(f,buffer,80);
		while(nb>0){
			int idx = 0;
			while(idx<nb) {
				(*car)++;
				if(isspace(buffer[idx]) && !isspace(last)){
					(*mot)++;
					if(buffer[idx]=='\n'){
						(*lig)++;
					}
				}
				last = buffer[idx++];
			}
			nb = read(f,buffer,80);
		}               
	}
	return 0;
}
