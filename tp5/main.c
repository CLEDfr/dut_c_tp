#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "wordCount.h"
#include <stdbool.h>

void erreur() {
	printf("Erreur : ./wc [ -l | -w | -c ] filename\n");
	exit(1);
}

int main(int argc, char **argv) {
	int fd;
	int res = 1;
	bool wB=false;bool cB=false; bool lB=false;
	int idx = 1;
	while(idx<argc && *argv[idx]=='-') {
		int i = 1;
		while(argv[idx][i]!='\0') {
			if(argv[idx][i]=='w') wB=true;
			else if(argv[idx][i]=='c') cB = true;
			else if(argv[idx][i]=='l') lB = true;
			else erreur();
			i++;
		}
		idx++;
	}
	int word = 0, car = 0, line = 0;
	while(idx<argc) {
		fd = open(argv[idx], O_RDONLY);
		if (fd == -1) {
			erreur();
		}
		int w=0, c=0, l=0;
		res = traiter(fd, &c, &w, &l);
		if(cB || (!wB && !lB)) printf("caractères : %d ", c);
		if(wB || (!cB && !lB)) printf("mots : %d ", w);
		if(lB || (!cB && !wB)) printf("lignes : %d ", l);
		printf("--> %s\n", argv[idx]);
		line+=l;word+=w;car+=c;
		l=0;w=0;c=0;
		idx++;
	}
	if(argc>2) {
		printf("\n");
		if(cB || (!wB && !lB)) printf("caractères : %d ", car);
		if(wB || (!cB && !lB)) printf("mots : %d ", word);
		if(lB || (!cB && !wB)) printf("lignes : %d ", line);
		printf("--> total\n");
	}
	return res;
}
