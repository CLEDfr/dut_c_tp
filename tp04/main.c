#include <stdio.h>
#include<stdlib.h>
#include "tests.h"
#include <string.h>
#include <ctype.h>
#include "fonctions.h"

int main(int argc, char **argv){

char *c = "";
int etatM=0;
int etatS=0;
int etatString=0;
    for (int i = 1 ; i < argc ; ++i) {
        if (argv[i][0] == '-' && (etatM==0 || etatS==0)){
            for (int idx = 1 ; argv[i][idx]!= '\0' ; ++idx) {
                if(argv[i][idx] == 'm'){
                    etatM=1;
                }else if(argv[i][idx] == 's'){
                    etatS=1;
                }else{ return -1;
                }
            }
        }else if(argv[i][0] != '-' && argv[i][0] != '\0'){
            etatString = 1;
            c = argv[i];       
        }
    }

if(etatM != 0 && etatS != 0){
   c = miroir(saisie());
    printf("-MS OK %s  \n",c);
}else if(etatS != 0){
    c = saisie();
    printf("-S OK %s  \n",c);
}else if(etatM != 0 && etatString!=0){
    c = miroir(c);
     printf("-M OK %s  \n",c);
}
return 0;
}
