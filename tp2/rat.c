#include <stdio.h>
#include "rat.h"

struct rat rat_produit(struct rat n1, struct rat n2){
struct rat r= {n1.den*n2.den,n1.num*n2.num};
return r;
}

struct rat rat_somme(struct rat n1, struct rat n2){
struct rat r= {n1.den*n2.den,n1.num*n2.den+n1.den*n2.num};
return r;
}

struct rat rat_plus_petit(struct rat list[]){
    struct rat r ={0,1};
    struct rat tamp1,tamp2;
    int i = 0;
    while(list[i].den!= 0){
        struct rat facteur1= {r.den,r.den};
        tamp1 = rat_produit(facteur1,list[i]);
        struct rat facteur2= {list[i].den,list[i].den};
        tamp2 = rat_produit(facteur2,r);
        if(tamp1.num<tamp2.num){
            r=list[i];
        }   
        i++;
    }                
    return r;
}
struct rat rat_simplifie(struct rat rat){

  int a,b,r;

  if (rat.den<rat.num){
      b=rat.den;
      a=rat.num;
	}
    else{
       a=rat.den;
       b=rat.num;
    }
    
  while (b!=0){
    r=a%b;
    a=b;
    b=r;        
  }
   if(a<0){
    a = -a;
  }
  
  struct rat res;
  res.den=rat.den/a;
  res.num=rat.num/a;
  
  return res;
}
/*struct rat rat_simplifie(struct rat r) {
	struct rat res = {r.den, r.num};
    int div;
    if(r.den<res.num){
        div=res.den;
    }else{ 
        div=res.num;
        if(div<0){
            div=-div;
        }
    }
    while(div>1) {
        if(res.num%div == 0 && res.den%div == 0) {
			res.num = res.num/div;
			res.den = res.den/div;
		}
		div--;
    }
	return res;
}
*/
